package com.example.yilmazlar.myapplication;


import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;



public class ColorPickerDialogFragment extends DialogFragment{



    SeekBar mScrollBarRed;
    SeekBar mScrollBarBlue;
    SeekBar mScrollBarGreen;

    TextView mScrollValueRed;
    TextView mScrollValueGreen;
    TextView mScrollValueBlue;


    //User selected colors for custom color.
    private int mColorValueRed;
    private int mColorValueGreen;
    private int mColorValueBlue;

    Button mUpdateButton;
    Context context;
    
    private int be;




    // Interface for the listener
    interface ColorDialogSelectionListener {

        void colorSelected(int colorRed, int colorGreen, int colorBlue);

    }

    private LinearLayout areaColorView;
    private ColorDialogSelectionListener mListener;




    public static ColorPickerDialogFragment newInstance() {

        ColorPickerDialogFragment fragment = new ColorPickerDialogFragment();
        return fragment;

    }

    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);
        context = activity;





        if (activity instanceof ColorDialogSelectionListener) {

            mListener = (ColorDialogSelectionListener) activity;

        } else {

            throw new RuntimeException(activity.toString() + " must implement ColorDialogSelectionListener");

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Renk Ayarlamasını Yapınız.");

        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View v = inflater.inflate(R.layout.color_picker_rgb_scale, null);
        builder.setView(v);

        areaColorView = (LinearLayout) v.findViewById(R.id.ColorShow);


        mUpdateButton = (Button) v.findViewById(R.id.update_button);

        mScrollBarRed = (SeekBar) v.findViewById(R.id.scrollbar_red);
        mScrollBarBlue = (SeekBar)  v.findViewById(R.id.scrollbar_blue);
        mScrollBarGreen = (SeekBar)  v.findViewById(R.id.scrollbar_green);

        mScrollValueRed = (TextView) v.findViewById(R.id.scroll_bar_value_red);
        mScrollValueGreen = (TextView) v.findViewById(R.id.scrollbar_value_green);
        mScrollValueBlue = (TextView) v.findViewById(R.id.scrollbar_value_blue);

        // displays the total for Red scrollbar and sets color value.
        mScrollBarRed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int value, boolean b) {

                mScrollValueRed.setText("Red value: " + value);
                mColorValueRed = value;
                areaColorView.setBackgroundColor(getRGB());

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });

        // displays the total for the Green scrollbar and sets color value.

        mScrollBarGreen.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int value, boolean b) {

                mScrollValueGreen.setText("Green value: " + value);
                mColorValueGreen = value;
                areaColorView.setBackgroundColor(getRGB());

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // displays the total for the Blue scrollbar and sets color value.

        mScrollBarBlue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int value, boolean b) {

                mScrollValueBlue.setText("Blue value: " + value);
                mColorValueBlue = value;
                areaColorView.setBackgroundColor(getRGB());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });




        // Will Change the screen to the RGB color code.
        mUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               mListener.colorSelected(mColorValueRed, mColorValueGreen, mColorValueBlue);
               int colors= getRGB();

                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("intValue",colors);
                editor.commit();

                be =LoadPreferences();
               // Toast.makeText(getActivity(), String.valueOf(be), Toast.LENGTH_SHORT).show();
                getDialog().dismiss();
            }

        });

        return builder.create();
    }
    public int getRGB(){

        return Color.rgb(mColorValueRed, mColorValueGreen, mColorValueBlue);

    }


    public int LoadPreferences(){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        int myInt = preferences.getInt("intValue",0);
        return myInt;


    }

    }
