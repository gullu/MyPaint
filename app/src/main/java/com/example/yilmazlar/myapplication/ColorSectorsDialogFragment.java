package com.example.yilmazlar.myapplication;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by YILMAZLAR on 2.6.2017.
 */

public class ColorSectorsDialogFragment extends DialogFragment {

    private ColorSectorsDialogSelectionListener mListener;
    Context context;
    private int mColorValueCount;
    private int mBrushSizeCount;
    private CanvasView cv;


    interface ColorSectorsDialogSelectionListener {
        void colorSelected(int sectorCount );
        void setPaintWidth(int value);
    }



    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);
        context = activity;

        if (activity instanceof ColorPickerDialogFragment.ColorDialogSelectionListener) {

            mListener = (ColorSectorsDialogFragment.ColorSectorsDialogSelectionListener) activity;

        } else {
            throw new RuntimeException(activity.toString() + " must implement ColorDialogSelectionListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {



        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Çizim Ayarlamasını Yapınız.");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View v = inflater.inflate(R.layout.cizim_ayarlari, null);
        builder.setView(v);
        cv = (CanvasView) v.findViewById(R.id.signature_canvas);




        Button Save_Button = (Button) v.findViewById(R.id.k_button);
        SeekBar mScrollBarSectors = (SeekBar) v.findViewById(R.id.scrollar_sectors);
        final SeekBar mScrollBarBrushSize = (SeekBar)v.findViewById(R.id.scrollar_brush_size);
        final TextView mScrollValueSectors = (TextView) v.findViewById(R.id.scroll_bar_value_sectors);


        mScrollBarSectors.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                mColorValueCount=i;
                mScrollValueSectors.setText("Simetriklik Sayısı: " + i);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        mScrollBarBrushSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                mBrushSizeCount=i;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        Save_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mListener.setPaintWidth(mBrushSizeCount);
                mListener.colorSelected(mColorValueCount);
                getDialog().dismiss();
            }

        });

        return builder.create();

    }
    public static ColorSectorsDialogFragment newInstance() {

        ColorSectorsDialogFragment fragment = new ColorSectorsDialogFragment();
        return fragment;

    }

}
