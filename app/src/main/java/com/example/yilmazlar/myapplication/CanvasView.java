package com.example.yilmazlar.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;



public class CanvasView extends View  {



	private Bitmap mBitmap;
	private Canvas mCanvas;
	private Path mPath;
	Context context;
	private Paint mPaint;
	private float mX, mY;
	private static final float TOLERANCE = 10;
	private int renk_degeri;



	private final ArrayList<ArrayList<Point>> lines = new ArrayList<>();
	int sectorsCount ;
	int points_cnt;
	int lines_cnt;




	public CanvasView(Context c, AttributeSet attrs) {
		super(c, attrs);
		context = c;
		// we set a new Path
		mPath = new Path();
		// and we set a new Paint with the desired attributes
		createPaint(Color.GRAY);
	}

	public void createPaint(int color) {

		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setColor(color);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeWidth(4f);

	}


	public void setPaintColor(int color) {
		renk_degeri=color;
		if(mPaint!=null) mPaint.setColor(color);
		invalidate();
	}

	public  void undo(){
		if (lines != null && lines.size() > 0) {
			lines.remove(lines.size() - 1);
			invalidate();
		}
	}

	public void setPaintWidthPx(float widthPx) {
		if (widthPx > 0) {
			mPaint.setStrokeWidth(widthPx);
			invalidate();
		}
	}


	// override onSizeChanged
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		// your Canvas will draw onto the defined Bitmap
		mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		mCanvas = new Canvas(mBitmap);
	}

	// override onDraw

	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		// draw the mPath with the mPaint on the canvas when onDraw

		lines_cnt = lines.size();
		for (int i = 0; i < lines_cnt; i++) {
			ArrayList<Point> line = lines.get(i);
			points_cnt = line.size();
			switch (points_cnt) {
				case 0:

					throw new RuntimeException("line.size() == 0; i = " + i);

				case 1:

					Point point = line.get(0);
					float x = point.x;
					float y = point.y;
					int z = point.z;
					int t = point.t;
					canvas.drawLine(x, y, x, y, mPaint);
					drawSectors(canvas, x, y, x, y);
					break;

				default:
					for (int j = 0; j < points_cnt - 1; j++) {
						Point currentPoint = line.get(j);
						Point nextPoint = line.get(j + 1);

						float srcX = currentPoint.x;
						float srcY = currentPoint.y;
						float dstX = nextPoint.x;
						float dstY = nextPoint.y;
						canvas.drawLine(srcX, srcY, dstX, dstY,mPaint);
						drawSectors(canvas, srcX, srcY, dstX, dstY);
					}

			}
		}
	}


	// when ACTION_DOWN start touch according to the x,y values
	private void startTouch(float x, float y) {
		mPath.moveTo(x, y);
		mX = x;
		mY = y;
		ArrayList<Point> line = new ArrayList<>();
		line.add(new Point(mX , mY , renk_degeri, sectorsCount));
		lines.add(line);
	}
	// when ACTION_MOVE move touch according to the x,y values
	private void moveTouch(float x, float y) {
		float dx = Math.abs(x - mX);
		float dy = Math.abs(y - mY);
		if (dx >= TOLERANCE || dy >= TOLERANCE) {
			mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
			mX = x;
			mY = y;
		}
		lines.get(lines.size() - 1 ).add(new Point(mX, mY,renk_degeri,sectorsCount));
	}
	// when ACTION_UP stop touch
	private void upTouch() {
		mPath.moveTo(mX, mY);
	}


	public void clearCanvas() {
		lines.clear();
		invalidate();
	}



	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float x = event.getX();
		float y = event.getY();


		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			startTouch(x, y);
			invalidate();
			break;
		case MotionEvent.ACTION_MOVE:
			moveTouch(x, y);
			invalidate();
			break;
		case MotionEvent.ACTION_UP:
			upTouch();
			invalidate();
			break;
		}
		return true;
	}

	private void drawSectors(Canvas g, float srcX, float srcY, float dstX, float dstY) {

		float centerX = mCanvas.getWidth() / 2;
		float centerY = mCanvas.getHeight() / 2;

		srcX -= centerX;
		srcY -= centerY;
		dstX -= centerX;
		dstY -= centerY;

		for (int j = 1; j < sectorsCount; j++) {

			float angle = (float) Math.toRadians(360f / sectorsCount * j);


			float cos = (float) Math.cos(angle);
			float sin = (float) Math.sin(angle);

			float newSrcX = (int) (srcX * cos - srcY * sin) + centerX;
			float newSrcY = (int) (srcX * sin + srcY * cos) + centerY;
			float newDstX = (int) (dstX * cos - dstY * sin) + centerX;
			float newDstY = (int) (dstX * sin + dstY * cos) + centerY;

			Toast.makeText(context, String.valueOf(newSrcX) + "      "+ String.valueOf(newDstX), Toast.LENGTH_SHORT).show();

			g.drawLine(newSrcX, newSrcY, newDstX, newDstY,mPaint);

		}
	}

}