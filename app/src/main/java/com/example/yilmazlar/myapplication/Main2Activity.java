package com.example.yilmazlar.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , ColorPickerDialogFragment.ColorDialogSelectionListener ,
        ColorSectorsDialogFragment.ColorSectorsDialogSelectionListener  {


    private int colorr;
    private CanvasView cv;
    File file, f = null ;
    Uri bmpUri;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Drawing");
        setSupportActionBar(toolbar);


        cv = (CanvasView) findViewById(R.id.signature_canvas);
        cv.setPaintColor(LoadPreferences());



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.clear_canvas) {
            Log.i("--","menu clicked");
            cv.clearCanvas();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.edit) {

            ColorSectorsDialogFragment dia = ColorSectorsDialogFragment.newInstance();
            dia.show(getFragmentManager(), "Show dialog");

        } else if (id == R.id.color) {

            ColorPickerDialogFragment dialog = ColorPickerDialogFragment.newInstance();
            dialog.show(getFragmentManager(), "Show dialog");

        } else if (id == R.id.delete) {
            cv.clearCanvas();

        } else if (id == R.id.back) {
            cv.undo();

        } else if (id == R.id.nav_share) {

            cv.setDrawingCacheEnabled(true);
            Bitmap bitmap = cv.getDrawingCache();

            if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
                file = new File(android.os.Environment.getExternalStorageDirectory(), "MyMiPa");
                if (!file.exists()) {
                    file.mkdirs();
                }

                f = new File(file.getAbsolutePath() + file.separator + System.currentTimeMillis() + ".png");
            }
            bmpUri = null;
            try {
                FileOutputStream ostream = new FileOutputStream(f);
                bitmap.compress(Bitmap.CompressFormat.PNG, 10, ostream);
                ostream.close();
                bmpUri = Uri.fromFile(f);
                Toast.makeText(this, "Resim Galeriye Kaydedildi", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this, "Resim Kaydedilemedi", Toast.LENGTH_SHORT).show();
            }


        } else if (id == R.id.nav_send) {

           if(bmpUri==null)
               Toast.makeText(this, "Paylaşmak için ilk önce resiminizi kaydediniz ", Toast.LENGTH_LONG).show();
          else {

               Intent intent = new Intent();
               intent.setAction(Intent.ACTION_SEND_MULTIPLE);
               intent.putExtra(Intent.EXTRA_STREAM, bmpUri);
               intent.setType("image/*");
               intent = Intent.createChooser(intent, "Share images to..");
               startActivity(intent);
           }

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void colorSelected(int colorRed, int colorGreen, int colorBlue) {


        colorr = Color.rgb(colorRed,colorGreen,colorBlue);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("intValue",colorr);
        editor.commit();
        //cv.setPaintColor(colorr);
         cv.createPaint(colorr);
    }

     public  void setPaintWidth(int value){

         SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
         SharedPreferences.Editor editor = preferences.edit();
         editor.putInt("intWidth",value);
         editor.commit();
         cv.setPaintWidthPx(widthValue());
     }

     public  int widthValue(){
         SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
         int myInt = preferences.getInt("intWidth",123456);
         return myInt;
     }

     public int LoadPreferences(){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        int myInt = preferences.getInt("intValue",123456);
        return myInt;

    }


    @Override
    public void colorSelected(int sectorCount) {
        if(sectorCount==0)
            cv.sectorsCount=1;
        else
            cv.sectorsCount=sectorCount;
    }
}
